import React, { Fragment, useState } from 'react'
import './SignupPage_styles.css'; 

function ResetPass() {
    const [timer,setTimer] = useState(30)
  return (
    <Fragment>
        <div className="container">
        <div className="card">
      <div className="card-content">
       <img src="https://twinr.dev/wp-content/themes/twinr/assets/images/logo.svg" className='twinr-logo' alt="twinr-logo" />
        <p className="card-description">Reset Password</p>
        <p className='card-msg' >We will email you a link so you can log in without typing anything.</p>
        <div className='input-container' >
            <p className='inp-text' >Enter OTP</p>
            <input type="text" className='input' />
        </div>
        <div className='input-container' >
            <p className='inp-text' >Enter Password</p>
            <input type="text" className='input' />
            <div className='otp-timer' >
                <div className='otp-msg-resend' >
                    <p>Did'nt get OTP ?</p>
                    <p>Resend</p>
                </div>
                <p>00.{timer}</p>
            </div>
        </div>
        <button className="signup-button">Reset Password</button>
            </div>
        </div>
      </div>
    </Fragment>
  )
}

export default ResetPass