import React from 'react';
import './SignupPage_styles.css'; 
import {AiOutlineCheckCircle} from "react-icons/ai"
import { NavLink } from 'react-router-dom';

const Signup_page = () => {
  return (
    <div className="container">
        <div className="card">
      <div className="card-content">
       <img src="https://twinr.dev/wp-content/themes/twinr/assets/images/logo.svg" className='twinr-logo' alt="twinr-logo" />
        <p className="card-description">Welcome to Twinr!</p>
        <p className='card-msg' >Enter your info to get started</p>
        <div className='input-container' >
            <p className='inp-text' >Enter Full Name</p>
            <input type="text" className='input' />
        </div>
        <div className='input-container' >
            <p className='inp-text' >Email address*</p>
            <input type="text" className='input' />
        </div>
        <div className='input-container' >
            <p className='inp-text'>Password</p>
            <input type="text" className='input' />
            <div className='authenticate-pass' >
                <div className='auth-pass-msg-red' ><AiOutlineCheckCircle/> <p>Min. 8 Char</p> </div>
                <div className='auth-pass-msg-red'><AiOutlineCheckCircle/> <p>One Uppercase,Lowercase & Number</p> </div>
            </div>
        </div>
        <button className="signup-button">CREATE MY ACCOUNT</button>
      </div>
    </div>
    <div className='footer-option' >Already have an account ? <span className='signup-click' > <NavLink to="/login" > SignIn</NavLink> </span>  </div>
    </div>
  );
};

export default Signup_page ;
