import React, { Fragment } from 'react'
import './SignupPage_styles.css'; 
import { NavLink } from 'react-router-dom';

function ForgotPass() {
  return (
    <Fragment>
        <div className="container">
        <div className="card">
      <div className="card-content">
       <img src="https://twinr.dev/wp-content/themes/twinr/assets/images/logo.svg" className='twinr-logo' alt="twinr-logo"/>
        <p className="card-description">Forgot Password</p>
        <p className='card-msg' >We will email you a link so you can log in without typing anything.</p>
        <div className='input-container' >
            <p className='inp-text' >Email address*</p>
            <input type="text" className='input' />
        </div>
        <button className="login-button"> <NavLink to="/reset" > SEND EMAIL </NavLink> </button>
        <p className='go-back' > <NavLink to="/login" >Go Back</NavLink> </p>
            </div>
        </div>
      </div>
    
    

    </Fragment>
  )
}

export default ForgotPass