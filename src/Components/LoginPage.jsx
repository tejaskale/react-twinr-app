import React from 'react'
import './SignupPage_styles.css'; 
import {NavLink} from "react-router-dom";

function LoginPage() {
  return (
    <div>
        <div className="container">
        <div className="card">
      <div className="card-content">
       <img src="https://twinr.dev/wp-content/themes/twinr/assets/images/logo.svg" className='twinr-logo' alt="twinr-logo" />
        <p className="card-description">Log Into Twinr!</p>
        <p className='card-msg' >Enter your info to Login</p>
        <div className='input-container' >
            <p className='inp-text' >Email address*</p>
            <input type="text" className='input' />
        </div>
        <div className='input-container' >
            <p className='inp-text'>Password</p>
            <input type="text" className='input' />
        </div>
        <button className="login-button">Login</button>
        <p className='forgot-pass' > <NavLink to="/forgotpass" >Forgot your Password ?</NavLink> </p>
        <div className='card-footer' >
            <p>By using Twinr, you agree to the</p>
            <div className='terms-policy' >
                <p>Terms & Conditions</p> & <p>Privacy Policy</p>
            </div>
        </div>
      </div>
    </div>
    <div className='footer-option' >Already have an account ? 
    <span className='signup-click' >
      <NavLink to="/" >Sign Up</NavLink>
    </span> 
    </div>
    </div>
    </div>
  )
}

export default LoginPage