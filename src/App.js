import './App.css';
import ReactDOM from "react-dom/client";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Fragment } from "react";
import SignUp_page from "./Components/SignUp_page";
import LoginPage from "./Components/LoginPage";
import ForgotPass from "./Components/ForgotPass";
import ResetPass from "./Components/ResetPass";



function App() {
  return (
    <Fragment>
          {/* <SignUp_page/>
          <LoginPage/> */}
    {/* <ForgotPass/> */}
    {/* <ResetPass/> */}
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<SignUp_page/>}/>
          <Route path="/login" element={<LoginPage/>} />
          <Route path="/forgotpass" element={<ForgotPass/>} />
          <Route path="/reset" element={<ResetPass/>} />
      </Routes>
    </BrowserRouter>
    </Fragment>
  );
}

export default App;
